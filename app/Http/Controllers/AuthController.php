<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Http\Requests\RegisterRequest;


class AuthController extends Controller
{
    public function Login(Request $request)
    {

        $credentials = $request->only('email', 'password');

        try{
        
            if(Auth::attempt($credentials))
            {
            
                /** @var User $user */
                $user = Auth::user();
                $token = $user->createToken('api');

                return response([
                    'massage'=> 'Success',
                    'token' => $token->plainTextToken,
                    'user' => $user

                ]);

            }
        } catch(\Exception $exception) {
            return response([
                'massage'=> $exception ->getMessage()
        
            ],status:400);

        }

            return response([
                'massage'=> 'Invalid username /password'
            ],status:401);
    }

    public function user() 
    {

        return Auth::user();
    }

    public function register(Request $request) 
    {
        try{
            $user = User::create([

                'first_name'=> $request->input('first_name'),
                'last_name'=> $request->input('last_name'),
                'email'=> $request->input('email'),
                'password'=> Hash::make($request->input('password')),

            ]);

            return $user;
        } catch(\Exception $exception) {
            return response([
                'massage'=> $exception ->getMessage()
        
            ],status:400);

        }
    }
           
}
