# Auth Api

## Descripción 

Crear un api en Laravel, con “endpoints” para autenticar por usuario y contraseña. Crear las migraciones y “seeders” con algunos usuarios de prueba.

Los "endpoints" solamente son:

- /validate [POST] autenticar por usuario y contraseña
- /user [GET] Toda la informacion del usuario.

El usuario, tiene los campos de: email, name, password (Encrypted)


Parte de la prueba es asumir que validaciones y detalles se necesitan para estos requerimientos.
Utilizar Laravel 9 y MySQL 8 por favor.

Hacer un "fork" del proyecto y trabajar en su propio repositorio
